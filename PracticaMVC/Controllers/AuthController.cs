﻿using PracticaMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;


namespace PracticaMVC.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private CaminEntities db = new CaminEntities();

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string order, string search)
        {
            if (!ModelState.IsValid) 
            {
                return View(model); 
            }


            var check = db.LoginModel.Where(a => a.Name.Equals(model.Name) && a.Password.Equals(model.Password)).FirstOrDefault();

            if (check != null )
            {
                
                var identity = new ClaimsIdentity(new[] {
                        new Claim(ClaimTypes.Name, model.Name)
                    }, "ApplicationCookie");

                if (check.RoleID != null)
                {
                    identity.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, check.Roles.Name, ClaimValueTypes.String));

                }
                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;
                authManager.SignIn(identity);

                return Redirect(GetRedirectUrl(model.ReturnUrl));
        }

            ModelState.AddModelError("", "Incorect Login sau parola");

            return View(model);
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("index", "home");
            }
            return returnUrl;
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Auth");
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}