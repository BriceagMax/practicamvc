﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using PracticaMVC.Models;
using System.Net;

namespace PracticaMVC.Models.Extend
{
    public class CaminController : Controller
    {
        public ActionResult Index()
        {
            CaminEntities db = new CaminEntities();

            var camin = from a in db.Camin select a;

            camin = db.Camin.OrderBy(a => a.ID);

            return View(camin);
        }

        public ActionResult TeamDetails(int? id)
        {
            CaminEntities db = new CaminEntities();

            if (id == null)
            {
                return HttpNotFound();
            }
            Camin camin = db.Camin.Include(t => t.Studenti).Include(t => t.Camere).Include(t => t.Asezare).FirstOrDefault(t => t.ID == id);
            if (camin == null)
            {
                return HttpNotFound();
            }
            return View(camin);
        }

        public ActionResult Edit(int? id)
        {
            CaminEntities db = new CaminEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Asezare asezare = db.Asezare.Find(id);

            if (asezare == null)
            {
                return HttpNotFound();
            }
            return View(asezare);
        }

        [HttpPost]
        public ActionResult Edit(Asezare asezare)
        {
            CaminEntities db = new CaminEntities();

            if (ModelState.IsValid)
            {
                db.Entry(asezare).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("TeamDetails/" + asezare.Camin_ID);
            }
            return View(asezare);
        }

        public ActionResult PartialToEdit()
        {
            return PartialView();
        }
    }
}