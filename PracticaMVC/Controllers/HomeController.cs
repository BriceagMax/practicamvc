﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PagedList;
using PracticaMVC.Models;

namespace PracticaMVC.Controllers
{
    public class HomeController : Controller
    {
        #region Index View  // Вывод списка проживающих студентов, поиск по полям таблицы, сортировка, пагинация
        public ActionResult Index(string order, string search, int? page)
        {
            CaminEntities db = new CaminEntities();

            ViewBag.NumeSort = order == "Nume" ? "nume" : "Nume";
            ViewBag.PrenumeSort = order == "Prenume" ? "prenume" : "Prenume";
            ViewBag.DateSort = order == "DataNasterii" ? "data" : "DataNasterii";

            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; 

            var stud = from stu in db.Studenti.Include(a => a.Camin1).Include(a => a.Camere) select stu;

            if (search == null)
            {
                stud = db.Studenti.OrderByDescending(a => a.StudentID);
                page = 1;
            }
            else
            {
                stud = db.Studenti.Include(a => a.Facultatea1).Include(a => a.Camin1).Where(a => a.Nume.Contains(search)
                    || a.Prenume.Contains(search)
                    //|| a.IDNP.Contains(search)
                    || a.Domiciliu.Contains(search)
                    //|| a.Telefon.Contains(search)
                    || a.Facultatea1.Denumire.ToString().Contains(search)
                    || a.Grupa.Contains(search)
                    || a.Camin1.Denumire.Contains(search));
                ViewBag.CurrentFilter = search;
            }
            ViewBag.CurrentSort = order;

            switch (order)
            {
                case "prenume":
                   stud = stud.OrderByDescending(s => s.Prenume);
                    break;
                case "Prenume":
                    stud = stud.OrderBy(s => s.Prenume);
                    break;
                case "nume":
                    stud = stud.OrderByDescending(s => s.Nume);
                    break;
                case "Nume":
                    stud = stud.OrderBy(s => s.Nume);
                    break;
                case "data":
                    stud = stud.OrderByDescending(s => s.DataNasterii);
                    break;
                case "DataNasterii":
                    stud = stud.OrderBy(s => s.DataNasterii);
                    break;
                default:
                    stud = stud.OrderByDescending(s => s.StudentID);
                    break;
            }
            ViewBag.CurrentFilter = search;



            var studView = stud.ToList().ToPagedList(pageIndex, pageSize);
            
            return View(studView);
        }
        #endregion

        #region Create View - Get   //Вывод в списки факультетов и общежитий из таблиц
        [Authorize(Roles = "admin")]
        [HttpGet]
        public ActionResult Create()
        {
            CaminEntities db = new CaminEntities();
            var fac = new SelectList(db.Facultatea.ToList(), "Id", "Denumire");
            ViewData["facultateaToList"] = fac;
            var camin = new SelectList(db.Camin.ToList(), "ID", "Denumire");
            ViewData["caminToList"] = camin;
            //var camera= new SelectList(db.Camere.Where(x =>x.Status!="full").ToList(), "ID", "Numar_camera");
            //ViewData["cameraToList"] = camera;

            //SelectList camera = new SelectList(db.Camere, "ID", "Numar_camera");
            //ViewBag.Camera = camera;

            return View();
        }
        #endregion

        #region Create View - Post  // Запись данных в таблицу Studenti, обновление таблиц Camin и Camere
        //(сколько комнат свободно, в скольких комнатах есть места, изменение статуса комнаты)

        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(/*[Bind(Include = "StudentID,Nume,Prenume,IDNP,Domiciliu,DataNasterii,Telefon,Facultatea,Grupa,Camin,Camera")]*/ Studenti stud)
        {
            CaminEntities db = new CaminEntities();

            var fac = new SelectList(db.Facultatea.ToList(), "Id", "Denumire");
            ViewData["facultateaToList"] = fac;

            var camin = new SelectList(db.Camin.ToList(), "ID", "Denumire");
            ViewData["caminToList"] = camin;

            //var camera = new SelectList(db.Camere.Where(x => x.Status != "full").ToList(), "ID", "Numar_camera");
            //ViewData["cameraToList"] = camera;

            if (ModelState.IsValid)
            {
                var cameraID = db.Camere.Where(c => c.ID == stud.Camera).FirstOrDefault();
                if (cameraID != null)
                {
                    int numStud = cameraID.Numar_student;
                    numStud += 1;
                    cameraID.Numar_student = numStud;
                    if (cameraID.Capacitate == cameraID.Numar_student)
                    {
                        cameraID.Status = "full";
                    }
                    else if (cameraID.Capacitate == 0)
                    {
                        cameraID.Status = "empty";
                    }
                    else 
                    {
                        cameraID.Status = "semifull";
                    }
                }

                var caminID = db.Camin.Where(c => c.ID == stud.Camin).FirstOrDefault();
                if (caminID != null)
                { 
                    int a = caminID.Numar_studente;
                    a += 1;
                    caminID.Numar_studente = a;
                    int b = caminID.Locuri_libere;
                    b -= 1;
                    caminID.Locuri_libere = b;
                    //if (cameraID.Status == "full")
                    //{
                    //    int count = caminID.Camere_Ocupate;
                    //    count += 1;
                    //    caminID.Camere_Ocupate = count;
                    //}
                }
                int d = 0;
                Asezare asezare = new Asezare
                {
                    Camin_ID = caminID.ID,
                    Camera_Id = cameraID.ID,
                    Student_ID = stud.StudentID,
                    Cazare_Debit = cameraID.Pret,
                    Cazare_Credit = d,
                    Plata_Lumina = d
                };

                db.Asezare.Add(asezare);
                db.Studenti.Add(stud);
                db.SaveChanges();


                return RedirectToAction("Index");
            }

            return View(stud);
        }
        #endregion

        #region Edit View - Get // Редактирование данных
        public ActionResult Edit(int? id)
        {
            CaminEntities db = new CaminEntities();

            var fac = new SelectList(db.Facultatea.ToList(), "Id", "Denumire");
            ViewData["facultateaToList"] = fac;

            var camin = new SelectList(db.Camin.ToList(), "ID", "Denumire");
            ViewData["caminToList"] = camin;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Studenti stud = db.Studenti.Find(id);


            if (stud == null)
            {
                return HttpNotFound();
            }
            else
            { 
                var cameraDel = db.Camere.Where(c => c.ID == stud.Camera).FirstOrDefault();
                if (cameraDel != null)
                {
                    int numStud = cameraDel.Numar_student;
                    numStud -= 1;
                    cameraDel.Numar_student = numStud;
                    if (cameraDel.Capacitate == cameraDel.Numar_student)
                    {
                        cameraDel.Status = "full";
                    }
                    else if (cameraDel.Capacitate == 0)
                    {
                        cameraDel.Status = "empty";
                    }
                    else
                    {
                        cameraDel.Status = "semifull";
                    }
                }

                var caminDel = db.Camin.Where(c => c.ID == stud.Camin).FirstOrDefault();
                if (caminDel != null)
                {
                    int a = caminDel.Numar_studente;
                    a -= 1;
                    caminDel.Numar_studente = a;
                    int b = caminDel.Locuri_libere;
                    b += 1;
                    caminDel.Locuri_libere = b;
                }
                db.SaveChanges();
            }  

            return View(stud);
        }
        #endregion

        #region Edit View - Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Studenti stud)
        {
            CaminEntities db = new CaminEntities();

            var fac = new SelectList(db.Facultatea.ToList(), "Id", "Denumire");
            ViewData["facultateaToList"] = fac;

            var camin = new SelectList(db.Camin.ToList(), "ID", "Denumire");
            ViewData["caminToList"] = camin;  

            if (ModelState.IsValid)
            {              
                var cameraID = db.Camere.Where(c => c.ID == stud.Camera).FirstOrDefault();
                if (cameraID != null)
                {
                    int numStud = cameraID.Numar_student;
                    numStud += 1;
                    cameraID.Numar_student = numStud;
                    if (cameraID.Capacitate == cameraID.Numar_student)
                    {
                        cameraID.Status = "full";
                    }
                    else if (cameraID.Capacitate == 0)
                    {
                        cameraID.Status = "empty";
                    }
                    else 
                    {
                        cameraID.Status = "semifull";
                    }
                    
                }

                var caminID = db.Camin.Where(c => c.ID == stud.Camin).FirstOrDefault();
                if (caminID != null)
                {
                    int a = caminID.Numar_studente;
                    a += 1;
                    caminID.Numar_studente = a;
                    int b = caminID.Locuri_libere;
                    b -= 1;
                    caminID.Locuri_libere = b;
                    //if (cameraID.Status == "full")
                    //{
                    //    int count = caminID.Camere_Ocupate;
                    //    count += 1;
                    //    caminID.Camere_Ocupate = count;
                    //}
                }

                var asezare = db.Asezare.Where(c => c.Student_ID == stud.StudentID).FirstOrDefault();
                {
                    if(asezare != null)
                    {
                        asezare.Camin_ID = caminID.ID;
                        asezare.Camera_Id = cameraID.ID;
                    }
                }
                db.Entry(stud).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(stud);
        }
        #endregion

        #region Delete View - Get   //Вывод информации в представление перед удалением

        public ActionResult Delete(int? id)
        {
            CaminEntities db = new CaminEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenti stud = db.Studenti.Find(id);
            if (stud == null)
            {
                return HttpNotFound();
            }
            return View(stud);
        }
        #endregion

        #region Delete View - Post удаление данных в таблице Studenti, обновление данных в таблицах Camin и Camere
        //При удалении уменьшаем количество студентов в общежитии, удаляем из комнаты, изменяется статус комнаты...
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CaminEntities db = new CaminEntities();
            Studenti stud = db.Studenti.Find(id);

            var cameraID = db.Camere.Where(c => c.ID == stud.Camera).FirstOrDefault();
            if (cameraID != null)
            {
                int numStud = cameraID.Numar_student;
                numStud -= 1;
                cameraID.Numar_student = numStud;
                if (cameraID.Capacitate == cameraID.Numar_student)
                {
                    cameraID.Status = "full";
                }
                else if (cameraID.Capacitate == 0)
                {
                    cameraID.Status = "empty";
                }
                else
                {
                    cameraID.Status = "semifull";
                }  
            }

            var caminID = db.Camin.Where(c => c.ID == stud.Camin).FirstOrDefault();
            if (caminID != null)
            {
                int a = caminID.Numar_studente;
                a -= 1;
                caminID.Numar_studente = a;
                int b = caminID.Locuri_libere;
                b += 1;
                caminID.Locuri_libere = b;
                //if (cameraID.Status != "full")
                //{
                //    int count = caminID.Camere_Ocupate;
                //    count -= 1;
                //    caminID.Camere_Ocupate = count;
                //}
            }
            var asezare = db.Asezare.Where(c => c.Student_ID == stud.StudentID).FirstOrDefault();

            db.Asezare.Remove(asezare);
            db.Studenti.Remove(stud);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Dispose Database    //Освобождаем неуправляемые ресурсы
        protected override void Dispose(bool disposing)
        {
            CaminEntities db = new CaminEntities();
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Dynamic list to Camera  //Частичное представление для динамического отображения комнат относительно выбранного общежития
        public ActionResult GetCameraList(int Camin)
        {
            CaminEntities db = new CaminEntities();
            List<Camere> camera = db.Camere.Where(x => x.Camin_ID == Camin && x.Status != "full").ToList();
            ViewBag.CameraID = new SelectList(camera, "ID", "Numar_camera");
            return PartialView("DisplayCamera");
        }
        #endregion


        public ActionResult Details(int? id)
        {
            CaminEntities db = new CaminEntities();
            if (id == null)
            {
                return HttpNotFound();
            }

            Studenti stud = db.Studenti.Include(t => t.Camin1).Include(t => t.Camere).Include(t => t.Asezare).FirstOrDefault(t => t.StudentID == id);
            if (stud == null)
            {
                return HttpNotFound();
            }
            return View(stud);
        }
    }
}