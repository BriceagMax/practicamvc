﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PracticaMVC.Models
{
    [MetadataType(typeof(AsezareData))]
    public partial class Asezare
    {

    }

    public class AsezareData
    {
        [Display(Name = "Cazare_Credit")]
        [Range(1, int.MaxValue, ErrorMessage = "Numai valori pozitive")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public int Cazare_Credit{ get; set; }

        [Display(Name = "Camera")]
        [Range(1, int.MaxValue, ErrorMessage = "Numai valori pozitive")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public int Plata_Lumina { get; set; }
    }
}
