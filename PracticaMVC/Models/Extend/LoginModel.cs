﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using PracticaMVC.Models;

namespace PracticaMVC.Models
{

    [MetadataType(typeof(UserData))]
    public partial class LoginModel
    {
        [HiddenInput(DisplayValue = false)]
        public int UserId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Email { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string ReturnUrl { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Nullable<int> RoleID { get; set; }
    }

    public class UserData
    {
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }      
    }
}