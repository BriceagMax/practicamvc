﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using PracticaMVC.Models;

namespace PracticaMVC.Models
{
    [MetadataType(typeof(StudentData))]
    public partial class Studenti
    {

    }

    public class StudentData
    {
        [Display(Name = "Nume")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Lungimea cimpului trebuie sa fie de la 3 pina la 20 caractere")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string Nume { get; set; }

        [Display(Name = "Prenume")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Lungimea cimpului trebuie sa fie de la 3 pina la 20 caractere")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string Prenume { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Numai valori pozitive")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "Cimpul IDNP trebuie se contine 13 caractere")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string IDNP { get; set; }

        [Display(Name = "DataNasterii")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        [DataType(DataType.Date)]
        public DateTime DataNasterii { get; set; }

        [Display(Name = "Telefon")]
        [StringLength(13, MinimumLength = 9, ErrorMessage = "Lungimea cimpului trebuie sa fie de la 9 pina la 13 caractere")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string Telefon { get; set; }

        [Display(Name = "Facultatea")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string Facultatea { get; set; }

        [Display(Name = "Grupa")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public string Grupa { get; set; }

        [Display(Name = "Camin")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public int Camin { get; set; }

        [Display(Name = "Camera")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Cimpul trebuie completat!")]
        public int Camera { get; set; }
    }

}