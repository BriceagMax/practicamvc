//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PracticaMVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Studenti
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Studenti()
        {
            this.Asezare = new HashSet<Asezare>();
        }
    
        public int StudentID { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string IDNP { get; set; }
        public string Domiciliu { get; set; }
        public System.DateTime DataNasterii { get; set; }
        public string Telefon { get; set; }
        public int Facultatea { get; set; }
        public string Grupa { get; set; }
        public int Camin { get; set; }
        public int Camera { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asezare> Asezare { get; set; }
        public virtual Camere Camere { get; set; }
        public virtual Camin Camin1 { get; set; }
        public virtual Facultatea Facultatea1 { get; set; }
    }
}
